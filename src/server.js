const express = require('express');
const converter = require('./converter');
const app = express();
const port = 3000;

//Welcome endpoint
app.get('/', (req, res) => res.send("Welcome"));

// RGB to Hex endpoint
app.get('/rgb-to-hex', (req, res) => {
    const red = parseInt(req.query.r);
    const green = parseInt(req.query.g);
    const blue = parseInt(req.query.b);
    const hex = converter.rgbToHex(red,green,blue);
    res.send(hex);
});

console.log("NODE_ENV: " + process.env.NODE_ENV);
if (process.env.NODE_ENV === 'test') {
    module.exports = app;
} else {
    app.listen(port, () => console.log("Server listening"));
}